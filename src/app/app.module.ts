import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { HttpModule } from '@angular/http';
import { StoreModule } from '@ngrx/store';

import {placeReducer} from '../store/reducers/place.reducers';
import { EffectsModule } from '@ngrx/effects';
import { PlaceEffects } from '../store/effects/place.effects';
import {StoreDevtoolsModule } from '@ngrx/store-devtools';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    StoreModule.forRoot({
      place: placeReducer
    }),
    EffectsModule.forRoot([ PlaceEffects ]),
    StoreDevtoolsModule.instrument({
      maxAge: 5
    })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
