import * as placeActions from '../actions/place.actions';
import * as actionTypes from '../actions/actionTypes';

export type Action = placeActions.All;
const iState = {
  wiadomosc: '',
  loading: false
};

export function placeReducer(state = iState, action: Action) {

  switch (action.type) {
    case actionTypes.GET_PLACE: {
      return {
        ...state
      };
    }
    case actionTypes.GET_PLACE_SUCCESS: {
      return {
        ...state
      };
    }
    default: {
      return state;
    }
  }
}
