import * as actionTypes from '../actions/actionTypes';
import * as postActions from '../actions/place.actions';
import { Effect, Actions, ofType,  } from '@ngrx/effects';
import {map, mergeMap} from 'rxjs/operators';
import { Http } from '@angular/http';
export type Action = postActions.All;
import {environment} from '../../environments/environment';

export class PlaceEffects {

  constructor(private actions: Actions, private http: Http ) { }
  @Effect()
  getPlace = this.actions
    .pipe(
      ofType(actionTypes.GET_PLACE),
      map((a: postActions.GetPlace) => a.payload),
      mergeMap(payload => this.http.get(environment.url)),
      map(res => res.json())


    );


}
