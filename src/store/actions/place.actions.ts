import { Action } from '@ngrx/store';
import * as actionTypes from '../actions/actionTypes';

export class GetPlace implements Action {
  readonly type = actionTypes.GET_PLACE;

  constructor(public payload: string) { }
}
export class GetPlaceSuccess implements Action {
  readonly type = actionTypes.GET_PLACE_SUCCESS;

  constructor(public payloade: string) { }
}

export type All = GetPlace | GetPlaceSuccess;
